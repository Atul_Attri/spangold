/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/7/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
var adItems = {
    "landing_page_main_scroller" : [{
        "href" : "#",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanwelcomebanner.jpg"
    },{
        "href" : "#/cat/2.6.11",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/pumanew.jpg"
    },{
        "href" : "#/cat/2.4.13",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spangoldbanner.jpg"
    },{
        "href" : "#/cat/6.3.2",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanbajaj.jpg"
    }, {
        "href" : "#/premium",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/spanbanner/spanpremiumbanner.jpg"
    }, {
        "href" : "#/cat/3.2.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/womens/Women_Apearals_saares.jpg"
    }],
    "landing_page_static_html" : "https://s3-ap-southeast-1.amazonaws.com/annectos/ads/men_static_ad.html",
    "offer_items" : [{
        "id" : "001",
        "offer_text" : "Only till 24th July midnight | Additional 10% off on Nike Shoes only for Intel employees",
        "href" : "#/cat/1.0/"
    }, {
        "id" : "002",
        "offer_text" : "Additional 5% off on Women Hand Bags only for Intel employees",
        "href" : "#/cat/2.0/"
    }],
    "l1_banner" : [{
        "id" : "men",
        "href" : "#/cat/2.4.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/accessoriceredeemnow.jpg"
    }, {
        "id" : "men",
        "href" : "#/cat/2.7.14",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/shirtsredeem.jpg"
    }, {
        "id" : "men",
        "href" : "#/cat/2.6.11",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/shoesbannerredeem.jpg"
    }, {
        "id" : "kids",
        "href" : "#/cat/4.2.2",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/kids_reedem.jpg"
    }, {
        "id" : "kids",
        "href" : "#/cat/4.2.2",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/kids2_reedem.jpg"
    },  {
        "id" : "kids",
        "href" : "#/cat/4.2.2",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/kids3_reedem.jpg"
    }, {
        "id" : "mt",
        "href" : "#/cat/7.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/casencoverrd.jpg"
    }, {
        "id" : "mt",
        "href" : "#/cat/7.1.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/mobilerd.jpg"
    },{
        "id" : "mt",
        "href" : "#/cat/7.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/tabletsaccessoriesrd.jpg"
    }, {
        "id" : "hk",
        "href" : "#/cat/6.4.3",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/Appliancesredeem.jpg"
    },{
        "id" : "hk",
        "href" : "#/cat/6.5.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/Chocolateredeemnow .jpg"
    }, {
        "id" : "hk",
        "href" : "#/cat/6.3.10",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/Microwaveableredeem.jpg"
    }, {
        "id" : "etc",
        "href" : "#/cat/5.2.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/electronic_reedem.jpg"
    }, {
        "id" : "etc",
        "href" : "#/cat/5.2.3",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/electronic3_reedem.jpg"
    },{
        "id" : "etc",
        "href" : "#/cat/5.2.7",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/electronics2_reedem.jpg"
    },{
        "id" : "women",
        "href" : "#/cat/3.6.0",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/Accessorieswmredeem.jpg"
    }, {
        "id" : "women",
        "href" : "#/cat/3.5.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/PersonalCareredeem.jpg"
    }, {
        "id" : "women",
        "href" : "#/cat/3.2.1",
        "link" : "http://cdn-new-annectos.s3.amazonaws.com/images/bigbanner/eselect/sarrebannersredeemnow.jpg"
    }]
};

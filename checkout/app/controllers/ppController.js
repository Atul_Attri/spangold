/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 1:08 PM
 * To change this template use File | Settings | File Templates.
 */

app.controller('PpController', function ($scope, $http, $q, $location, $window, utilService, $rootScope,
                                         $routeParams, ppService, localStorageService, cartService, acctService, giftService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below

    $scope.isClickable = false;

    function check_login() {
        if (localStorageService.get('user_info') == null) {
            $window.location.href = root_url + "/index.html#/login";
        }
        else {
            $window.location.href = root_url + "/home.html";
        }
    }
    $scope.select_address = function (selected_address) {
        populate_selected_address(selected_address);
    }

    $scope.RedeemPoint = function () {

        
        var returnval = '';
        localStorageService.set ("express_shipping", 0);
        $scope.isClickable = true;
        if ($scope.balance_amount <= 0) {

            ppService.RedeemUserPoint($http, $q, $scope.pp).then(function (data) {
                acctService.set_points($scope.pp.Points); // change point balance
                localStorageService.set('local_cart', "[]");//reset cart
                var confirm_order_url = root_url + "/payment/payment.html#/payment/2";
                $window.location.href = confirm_order_url;
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }
        else {

            //alert (payment_gateway)  ;
            if (payment_gateway === 0){
                alert ("Please use your points to make payment")
            }
            else {
                $('#myform').submit();
            }
        }
    }

    var order_id;
    var gv = {
        id: "",
        gift_certificate_code: "",
        amount: 0
    }
    var gv_list = [];
    for (i = 1; i <= 5; i++) {
        var g = {
            id: i.toString(),
            amount: 0
        };//new gv;//.gift_certificate_code= i.toString();
        gv_list.push(g);
    }
    //console.log(gv_list);
    $scope.gv_list = gv_list;
    var conv_ratio = utilService.get_conversion_ratio();
    $scope.conv_ratio = conv_ratio;
    $scope.store_type = utilService.get_store_type();

    function init() {
        //        console.log ($(document.location));

        // get order_id
        order_id = $routeParams.order_id;
        if  (localStorageService.get('express_shipping') != null)
        {
            $scope.express_shipping  =localStorageService.get('express_shipping');
        }
        else {
            $scope.express_shipping  = 0;
        }
//        if (cartService.express_shipping){
//            $scope.express_shipping = 150; // todo - put to config
//        }
//        else {
//            $scope.express_shipping = 0;
//        }


        $scope.payment_gateway = utilService.get_payment_gateway();

        // get selected address
        $scope.selected_address = localStorageService.get('selected_ship_address');;
        $scope.gv_total = 0;


        $scope.user_point = parseInt(acctService.get_points()).toFixed(0);


        $scope.point_amount = ($scope.user_point / conv_ratio).toFixed(0);
        $scope.redeem_point = 0;

        $scope.redeem_point_rs = 0;//($scope.redeem_point  / conv_ratio).toFixed(0)
        $scope.total_cart_amount = cartService.get_cart_amount();
        $scope.balance_amount = $scope.total_cart_amount + parseInt($scope.express_shipping);
        $scope.discount_coupon_total = 0;
        $scope.discount_coupon = "";

        // pre-populate with point available
        // added by rahul on 13th Feb
        if ($scope.point_amount >= $scope.balance_amount){
            $scope.redeem_point =  $scope.balance_amount;
        }
        else {
            $scope.redeem_point =  $scope.point_amount;
        }
        $scope.redeem_point_rs =  ($scope.redeem_point / conv_ratio).toFixed(0);
        $scope.balance_amount = $scope.total_cart_amount + parseInt($scope.express_shipping) - $scope.redeem_point_rs;
        $scope.pp = {};
//        $scope.redeem_points();
        calc_balance();

        $scope.pp.total_amount = $scope.balance_amount;//cartService.get_cart_amount();
        $scope.pp.actual_amount = $scope.total_cart_amount;
        $scope.pp.Order_Id = $routeParams.order_id;
        $scope.pp.Points = $scope.redeem_point;
        $scope.pp.Points_Inr = $scope.redeem_point_rs;
        $scope.pp.billing_cust_name = $scope.selected_address.name;
        $scope.pp.billing_cust_address = $scope.selected_address.shipping_name;
        $scope.pp.billing_cust_state = $scope.selected_address.state;
        $scope.pp.billing_cust_tel = $scope.selected_address.mobile_number;
        $scope.pp.billing_cust_email = localStorageService.get('user_info').email_id;
        $scope.pp.name = $scope.selected_address.name;
        $scope.pp.address = $scope.selected_address.shipping_name;
        $scope.pp.state = $scope.selected_address.state;
        $scope.pp.mobile_number = $scope.selected_address.mobile_number;
        $scope.pp.billing_cust_city = $scope.selected_address.city;
        $scope.pp.billing_zip_code = $scope.selected_address.pincode;
        $scope.pp.delivery_cust_city = $scope.selected_address.city;
    }
    var selected_address = {};
    var apply_rules = function(){

        if (discount == null){
            $scope.discount_validation_msg = "Coupon not valid."  ;
            $scope.discount_coupon_total = 0;
        }
        else {
            if (discount.min_value <=$scope.total_cart_amount ){
                if (discount.discount_type == "f"){
                    // flat off
                    $scope.discount_coupon_total = discount.discount_value;
                }
                else {
                    // % off
                    var off =  ( $scope.total_cart_amount *  (1-((100-discount.discount_value)/100))).toFixed(0);
                    //console.log(discount.max_discount);
                    if (off <= discount.max_discount){
                        $scope.discount_coupon_total = off;
                    }
                    else {
                        $scope.discount_coupon_total =    discount.max_discount;
                    }
                }

            }
            else {
                $scope.discount_validation_msg = "Min purchase criteria not met"  ;
            }
        }


    }
    init();
    $scope.validate_gv = function (gv, idx) {
        // first check for dup
        //var tmp = _.where($scope.gv_list, {gift_voucher_code:gv})  ;
        var l_found_count = 0;
        for (i = 0; i < $scope.gv_list.length; i++) {
            if ($scope.gv_list[i].gift_certificate_code === gv) {
                l_found_count = l_found_count + 1;
            }
        }

        if (l_found_count > 1) {
            $scope.msg = "Duplicate Gift Voucher";
        }
        else {
            // check for validity
            $scope.msg = "";
            giftService.validate_gv($http, $q, gv).then(function (data) {

                $scope.gv_list[idx] = data;
                $scope.gv_list[idx].gift_certificate_code = gv;
                $scope.gv_list[idx].company = utilService.get_store();
                console.log($scope.gv_list);
                if ($scope.gv_list[idx].amount == 0) {
                    $scope.msg = "Invalid Gift Voucher";
                }
                //console.log($scope.gv_list)  ;
                calc_balance();
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }
    }

    var discount_coupon_validation = function(){
        if ($scope.discount_coupon ===""){
            $scope.discount_coupon_total = 0;
            $scope.discount_validation_msg = ""  ;
            calc_balance();
        }
        else{
        ppService.validate_discount_coupon($http, $q, $scope.discount_coupon, utilService.get_store(), localStorageService.get('user_info').email_id).then(function (data) {
                console.log(data);
                discount = JSON.parse(data.rule);
                apply_rules();
                //console.log($scope.discount_validation_msg);
                //console.log($scope.gv_list)  ;
                calc_balance();
            },
            function () {
                //Display an error message
                $scope.error = error;
            });
        }
    }
    $scope.apply_coupon = function(){
       discount_coupon_validation();
    }


    $scope.validate_discount_point = function () {
        discount_coupon_validation();

    }

    $scope.redeem_points = function () {

        //console.log("redeem_point");
        if (parseInt($scope.redeem_point) > parseInt($scope.user_point)) {
            $scope.redeem_point = $scope.user_point
        }
        calc_balance();
        //        var old = $scope.old_redeem_point;
        //        calc_balance();
        //        if ($scope.balance_amount <= 0){
        //            $scope.redeem_point =  $scope.old_redeem_point;
        //        }


    }
    $scope.remove_gv = function (idx) {
        $scope.gv_list[idx] = { id: idx, gift_certificate_code: "", amount: 0 };
        calc_balance();
        //console.log($scope.gv_list);


    }

    function calc_balance() {
        var certcode = '';
        $scope.redeem_point_rs = ($scope.redeem_point / conv_ratio).toFixed(0)
        $scope.total_cart_amount = cartService.get_cart_amount();
        var gv_total = 0;
        for (i = 0; i < 5; i++) {

            if ($scope.gv_list[i].amount != null) {
                gv_total = gv_total + $scope.gv_list[i].amount;

                if (certcode == '') {
                    if ($scope.gv_list[i].gift_certificate_code != null)// || typeof($scope.gv_list[i].gift_certificate_code) != 'undefined') {
                    {
                        certcode = $scope.gv_list[i].gift_certificate_code;
                    }
                }
                else {
                    if ($scope.gv_list[i].gift_certificate_code != null)  // || typeof($scope.gv_list[i].gift_certificate_code) != 'undefined') {
                    {
                        certcode = certcode + '|' + $scope.gv_list[i].gift_certificate_code;
                    }
                }
            }
        }

        $scope.gv_total = gv_total;
        //if ($scope.discount_coupon_total != 0){

            $scope.balance_amount = $scope.total_cart_amount + parseInt($scope.express_shipping) -$scope.discount_coupon_total - $scope.redeem_point_rs - gv_total;
            if ($scope.balance_amount < 0){
                if ($scope.redeem_point_rs >= $scope.discount_coupon_total ){
                    $scope.redeem_point_rs =  $scope.redeem_point_rs - $scope.discount_coupon_total;
                    $scope.redeem_point = ($scope.redeem_point_rs * conv_ratio);

                }
//                else {
//                    $scope.redeem_point_rs = 0;
//                    $scope.redeem_point = 0;
//                }
            }

//        }
        $scope.balance_amount = $scope.total_cart_amount + parseInt($scope.express_shipping) -$scope.discount_coupon_total - $scope.redeem_point_rs - gv_total;

        if ($scope.balance_amount < 0) //For -ve balance
            $scope.balance_amount = 0;

       // $scope.old_redeem_point = $scope.redeem_point;
        $scope.pp.total_amount = $scope.balance_amount;
        $scope.pp.Points = $scope.redeem_point;
        $scope.pp.Points_Inr = $scope.redeem_point_rs;
        $scope.pp.EGift_VNO = certcode;
        $scope.pp.EGift_Amt = gv_total;
        $scope.pp.discount_coupon = $scope.discount_coupon;
        $scope.pp.discount_coupon_total = $scope.discount_coupon_total;

        //console.log($scope.pp);

    }

    $scope.route_to = function (page) {
        var url = "/" + page + "/" + order_id;
        $location.path(url);
    }

});
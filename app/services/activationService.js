﻿    app.service('activationService', function () {

        this.activation = function ($http, $q, dataobj) {
            var apiPath = cat_service_url +   '/user/registration/activate_registration';
            var deferred = $q.defer();


            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                    deferred.resolve(data);
                    //localStorageService.add('user_info', data[0]);
                }).error(function (data)
                {
                    deferred.reject("An error occured while validating User");
                })
            return deferred.promise;

//            $http.post(apiPath)
//                .success(function (data) {
//                    deferred.resolve(data);
//                })
//                .error(function () {
//                    deferred.reject("An error occured while fetching items");
//                });
//            return deferred.promise;
        };
    });





   
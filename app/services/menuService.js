/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 24/6/13
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
//This handles retrieving data and is used by controllers. 3 options (server, factory, provider) with
//each doing the same thing just structuring the functions/data differently.
app.service('menuService', function () {
    this.getMenuItems = function () {
        return  menuItems;
    };
    this.get_all_menu = function ($http, $q, company){
        //var apiPath = cat_service_url +  '/store/menu?json=true';
        var apiPath = cat_service_url +  '/store/menu/' + company + '?json=true';

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

        return deferred.promise;
    };
    this.get_level1_menu = function ($http, $q){
        var apiPath = cat_service_url +  '/store/level1menu?json=true';

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

        return deferred.promise;
    };
    this.get_menu_expiry = function ($http, $q, last_download_date){
        var apiPath = cat_service_url +  '/store/menu/expired?last_download_date=' +last_download_date +'&json=true';
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

        return deferred.promise;
    };
    this.getAds = function (cat_id) {
        return  menuItems[0].ad_urls;
    };

    this.insertMenuItems = function (id, name, parentId, description) {
        var topID = menuItems.length + 1;
        menuItems.push({
            id: "ABCDEF" + topID,
            Name: name,
            ParentId: parentId,
            description: description
        });
    };

    this.deleteMenuItems = function (id) {
        for (var i = menuItems.length - 1; i >= 0; i--) {
            if (menuItems[i].id === id) {
                menuItems.splice(i, 1);
                break;
            }
        }
    };

    this.getMenuItem = function (id) {
        for (var i = 0; i < menuItems.length; i++) {
            if (menuItems[i].id === id) {
                return menuItems[i];
            }
        }
        return null;
    };



});
﻿/// <reference path="../Scripts/angular-1.1.4.js" />

/*#######################################################################
  
  Rahul Guha

  Normally like to break AngularJS apps into the following folder structure
  at a minimum:

  /app
      /controllers      
      /directives
      /services
      /partials
      /views

  #######################################################################*/

var app = angular.module('ECOMM', ['ngRoute','ui.bootstrap', 'LocalStorageModule']);
app.run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeSuccess', function(){
        ga('send', 'pageview', $location.path());
    });
});
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        .when('/customers',
            {
                controller: 'CustomersController',
                templateUrl: '/app/partials/customers.html'
            })
        .when('/cart',
        {
            controller: 'cartController',
            templateUrl: '/app/partials/cart_page.html'
        })
        .when('/checkout',
        {
            controller: 'checkoutController',
            templateUrl: '/app/partials/checkout.html'
        })
          .when('/order',
        {
            controller: 'orderController',
            templateUrl: '/app/partials/order.html'
        })

      //.when('/payment',
      //  {
      //      controller: 'paymentController',
      //      templateUrl: '/app/partials/Payment.html'
      //  })
    
        .when('/home',
        {
            controller: 'HomeController',
            templateUrl: '/app/partials/home_dummy.html'
        })
//        .when('/checkout_switch',
//        {
//            controller: 'Checkout_switchController' ,
//            templateUrl: '/app/partials/home_dummy.html'
//        })
        .when('/landing',
        {
            controller: 'landingController',
            templateUrl: '/app/partials/landing.html'
        })
        .when('/registration',
        {
            controller: 'registrationController',
            templateUrl: '/app/partials/Registration.html'
        })
        .when('/login',
        {
            controller: 'loginController',
            templateUrl: '/app/partials/login.html'
        })

         .when('/forgot_password',
        {
            controller: 'forgotpwdController',
            templateUrl: '/app/partials/forgot_password.html'
        })

       .when('/message',
        {
            controller: 'messageController',
            templateUrl: '/app/partials/message.html'
        })

         .when('/activation/:id/:cid',
        {
            controller: 'activationController',
            templateUrl: '/app/partials/accountActivation.html'
        })
        .when('/reset_password/:id/:cid',
        {
            controller: 'reset_passwordController',
            templateUrl: '/app/partials/reset_password.html'
        })
        //Define a route that has a route parameter in it (:customerID)
        .when('/customerorders/:customerID',
            {
                controller: 'CustomerOrdersController',
                templateUrl: '/app/partials/customerOrders.html'
            })
        //Define a route that has a route parameter in it (:customerID)
        .when('/orders',
            {
                controller: 'OrdersController',
                templateUrl: '/app/partials/orders.html'
            })
        .when('/acct_points',
        {
            controller: 'acct_pointsController',
            templateUrl: '/app/partials/acct_points.html'
        })

        .when('/myorder',
        {
            controller: 'MyorderController',
            templateUrl: '/app/partials/myorder.html'
        })

        .when('/cat/:catId',
        {
            controller: 'CatController',
            templateUrl: '/app/partials/cat.html'
        })
        .when('/special/:catId',
        {
            controller: 'specialController',
            templateUrl: '/app/partials/special.html'
        })

        .when('/newarrivals',
        {
            controller: 'newarrivalsController',
            templateUrl: '/app/partials/newarrivals.html'
        })

        .when('/premium',
        {
            controller: 'premiumController',
            templateUrl: '/app/partials/premium.html'
        })

        .when('/express',
        {
            controller: 'expressController',
            templateUrl: '/app/partials/express.html'
        })

        .when('/cat1/:catId',
        {
            controller: 'Cat1Controller',
            templateUrl: '/app/partials/cat1.html'
        })
        .when('/brand/:brand_name',
        {
            controller: 'brandController',
            templateUrl: '/app/partials/brand.html'
        })
        .when('/cat/:cat_id/brand/:brand_name',
        {
            controller: 'CatController',
            templateUrl: '/app/partials/cat.html'
        })
        .when('/prod/:prodId',
        {
            controller: 'ProductDetailsController',
            templateUrl: '/app/partials/product.html'
        })
        .when('/search/:srch/:context',
        {
            controller: 'searchController',
            templateUrl: '/app/partials/search.html'
        })

        .when('/quickbuy/:prodId',
        {
            controller: 'quickbuyController',
            templateUrl: '/app/partials/quickbuy.html'
        })
        .when('/quickbuy/:prodId/:cart',
        {
            controller: 'quickbuyController',
            templateUrl: '/app/partials/quickbuy.html'
        })

        .when('/contact_us',
        {
            controller: 'contactUsController',
            templateUrl: '/app/partials/contact_us.html'
        })
        .when('/feedback',
        {
            controller: 'feedbackController',
            templateUrl: '/app/partials/feedback.html'
        })
        .when('/terms',
        {
//            controller: 'ProductDetailsController',
            templateUrl: '/app/partials/terms.html'
        })
        .when('/aboutus',
        {
            //controller: 'registrationController',
            templateUrl: '/app/partials/aboutus.html'
        })
        .when('/return_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/app/partials/return_policy.html'
        })

        .when('/security_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/app/partials/security_policy.html'
        })

        .when('/privacy_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/app/partials/privacy_policy.html'
        })
        .when('/wishlist',
        {
            controller: 'wishlistController',
            templateUrl: '/app/partials/wishlist.html'
        })



        .otherwise({ redirectTo: '/home'});
});





/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/7/13
 * Time: 5:48 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('offerController', function ($scope, $location, $http, $q,  offerService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below

    //var ads;// = adService.get_ads();

    function init() {
        $scope.offers = offerService.get_offers();
        //alert ($scope.offers[0].offer_text)
    }

    init();

});
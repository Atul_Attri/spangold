/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 9:18 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('Checkout_switchController', function ($scope, $location, $window) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    function check_login (){
        if (localStorageService.get('user_info') == null){
            $window.location.href = root_url + "/index.html#/login";
        }
        else
        {

            $window.location.href = root_url + "/home.html";
        }
    }
    function init() {
       
        check_login();
        // load pricing rules if not already loaded
//        utilService.load_rules();
        //load_rules();

        //$window.location = "/home.html";
    }
    init();
   
});
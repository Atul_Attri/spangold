/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 24/6/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
//This controller retrieves data from the cache/cat.json and associates it with the $scope
//The $scope is ultimately bound to the menu view
app.controller('MenuController', function ($scope, $location, $window,  menuService, $http, $q, localStorageService, utilService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    init();
    var menu;
    function init() {
        console.log(utilService.get_store())
        get_all_menu();
    }
    this.isLevel2 = function(val){
        if (val == 2) {
            return true;
        }
        else{
            return false;
        }
    }
    $scope.get_menu = function(name, level){
        //console.log(menuService.get_menu(name,level));
        menuService.get_menu(name,level);
    }

    $scope.open_level1_page = function(name){
        //console.log(menuService.get_menu(name,level));
        url = root_url + "/cat1.html?cat=" + name ;
        $window.location.href = url;
    }

    // new methods//
    function get_all_menu(){
        var expiry_status;
        var last_download_date;
        var tmp_menu =localStorageService.get("menu");
        if (localStorageService.get("menu_refresh")===null)
        {
            last_download_date = ""
        }
        else{
            last_download_date =  localStorageService.get("menu_refresh");
        }
        if (tmp_menu != null){
            // check expiry of menu
            menuService.get_menu_expiry($http, $q, last_download_date).then(function(data){
                var expiry_status = data;
                if (expiry_status > 0) {
                    // get menu again
                    menuService.get_all_menu($http, $q, utilService.get_store()).then(function(data){
                            menu = data;
                            //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                            var last_refresh = new XDate();

                            localStorageService.add("menu", menu);
                            localStorageService.add("menu_refresh", last_refresh.toDateString("MMM d, yyyy"));
                            //console.log(new XDate());
                            if (get_men_menu().length !=0) {
                                $scope.men_link = _.filter(menu,function(m){ return m.name === "men"})[0].id;
                                $scope.l2_men_menu = get_level2_men_menu();
                                $scope.l3_men_menu = get_level3_men_menu();
                            }
                            if (get_women_menu().length !=0) {
                                $scope.women_link = _.filter(menu,function(m){ return m.name === "women"})[0].id;
                                $scope.l2_women_menu = get_level2_women_menu();
                                $scope.l3_women_menu = get_level3_women_menu();
                            }
                            if (get_kids_menu().length !=0) {
                                $scope.kids_link = _.filter(menu,function(m){ return m.name === "kids"})[0].id;
                                $scope.l2_kids_menu = get_level2_kids_menu();
                                $scope.l3_kids_menu = get_level3_kids_menu();
                            }
                            if (get_etc_menu().length !=0) {
                                $scope.etc_link = _.filter(menu,function(m){ return m.name === "electronics & computers"})[0].id;
                                $scope.l2_etc_menu = get_level2_etc_menu();
                                $scope.l3_etc_menu = get_level3_etc_menu();
                            }

                            if (get_hk_menu().length !=0) {
                                $scope.hk_link = _.filter(menu,function(m){ return m.name === "home & kitchen"})[0].id;
                                $scope.l2_hk_menu = get_level2_hk_menu();
                                $scope.l3_hk_menu = get_level3_hk_menu();
                            }
                            if (get_mt_menu().length !=0) {
                                $scope.mt_link = _.filter(menu,function(m){ return m.name === "mobiles & tablets"})[0].id;
                                $scope.l2_mt_menu = get_level2_mt_menu();
                                $scope.l3_mt_menu = get_level3_mt_menu();
                            }
                        },
                        function(){
                            //Display an error message
                            $scope.error= error;
                        });

                }
                else {
                    //

                    menu = localStorageService.get("menu")   ;

                    if (get_men_menu().length !=0) {
                        $scope.l2_men_menu = get_level2_men_menu();
                        $scope.l3_men_menu = get_level3_men_menu();
                    }
                    if (get_women_menu().length !=0) {
                        $scope.l2_women_menu = get_level2_women_menu();
                        $scope.l3_women_menu = get_level3_women_menu();
                    }
                    if (get_kids_menu().length !=0) {
                        $scope.l2_kids_menu = get_level2_kids_menu();
                        $scope.l3_kids_menu = get_level3_kids_menu();
                    }
                    if (get_etc_menu().length !=0) {
                        $scope.l2_etc_menu = get_level2_etc_menu();
                        $scope.l3_etc_menu = get_level3_etc_menu();
                    }
                    if (get_hk_menu().length !=0) {
                        $scope.l2_hk_menu = get_level2_hk_menu();
                        $scope.l3_hk_menu = get_level3_hk_menu();
                    }
                    if (get_mt_menu().length !=0) {
                        $scope.l2_mt_menu = get_level2_mt_menu();
                        $scope.l3_mt_menu = get_level3_mt_menu();
                    }
                }

            });

        }
        else {
                //console.log("making call to get all menu");
                var company  =   localStorageService.get("user_info").company;
                menuService.get_all_menu($http, $q,company).then(function(data){
                menu = data;
                //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                    var refresh_date = new XDate();
                    localStorageService.add("menu", menu);
                    localStorageService.add("menu_refresh",refresh_date.toString("MMM d, yyyy"));  //new XDate());
                    //console.log(new XDate());
                    if (get_men_menu().length !=0) {
                        $scope.men_link = _.filter(menu,function(m){ return m.name === "men"})[0].id;
                        $scope.l2_men_menu = get_level2_men_menu();
                        $scope.l3_men_menu = get_level3_men_menu();
                    }
                    if (get_women_menu().length !=0) {
                        $scope.women_link = _.filter(menu,function(m){ return m.name === "women"})[0].id;
                        $scope.l2_women_menu = get_level2_women_menu();
                        $scope.l3_women_menu = get_level3_women_menu();
                    }
                    if (get_kids_menu().length !=0) {
                        $scope.kids_link = _.filter(menu,function(m){ return m.name === "kids"})[0].id;
                        $scope.l2_kids_menu = get_level2_kids_menu();
                        $scope.l3_kids_menu = get_level3_kids_menu();
                    }
                    if (get_etc_menu().length !=0) {
                        $scope.etc_link = _.filter(menu,function(m){ return m.name === "electronics & computers"})[0].id;
                        $scope.l2_etc_menu = get_level2_etc_menu();
                        $scope.l3_etc_menu = get_level3_etc_menu();
                    }
                    if (get_hk_menu().length !=0) {
                        $scope.hk_link = _.filter(menu,function(m){ return m.name === "home & kitchen"})[0].id;
                        $scope.l2_hk_menu = get_level2_hk_menu();
                        $scope.l3_hk_menu = get_level3_hk_menu();
                    }
                    if (get_mt_menu().length !=0) {
                        $scope.mt_link = _.filter(menu,function(m){ return m.name === "mobiles & tablets"})[0].id;
                        $scope.l2_mt_menu = get_level2_mt_menu();
                        $scope.l3_mt_menu = get_level3_mt_menu();
                    }
                },
                function(){
                    //Display an error message
                    $scope.error= error;
                });
        }

    }
    function get_men_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "men"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "men"});
        }
        else{
                return [];
            }

    }
    function get_women_menu(){

        var tmp = _.filter(menu,function(m){ return m.name === "women"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "women"});
        }
        else{
            return [];
        }
    }
    function get_kids_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "kids"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "kids"});
        }
        else{
            return [];
        }


    }
    function get_etc_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "electronics & computers"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "electronics & computers"});
        }
        else{
            return [];
        }
    }

    function get_hk_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "home & kitchen"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "home & kitchen"});
        }
        else{
            return [];
        }
    }

    function get_mt_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "mobiles & tablets"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "mobiles & tablets"});
        }
        else{
            return [];
        }
    }

    function  get_level2_men_menu (){
        return _.where( get_men_menu()[0].menu_items,  {level:2});
    }
    function get_level3_men_menu (){
        return _.where( get_men_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_women_menu (){
        return _.where( get_women_menu()[0].menu_items,  {level:2});
    }
    function get_level3_women_menu (){
        return _.where( get_women_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_kids_menu (){
        return _.where( get_kids_menu()[0].menu_items,  {level:2});
    }
    function get_level3_kids_menu (){
        return _.where( get_kids_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_etc_menu (){
        return _.where( get_etc_menu()[0].menu_items,  {level:2});
    }
    function get_level3_etc_menu (){
        return _.where( get_etc_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_hk_menu (){
        return _.where( get_hk_menu()[0].menu_items,  {level:2});
    }
    function get_level3_hk_menu (){
        return _.where( get_hk_menu()[0].menu_items,  {level:3});
    }

    function  get_level2_mt_menu (){
        console.log(get_mt_menu()[0].menu_items);
        return _.where( get_mt_menu()[0].menu_items,  {level:2});
    }
    function get_level3_mt_menu (){
        return _.where( get_mt_menu()[0].menu_items,  {level:3});
    }

});
